package ictgradschool.web.assignment04;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Servlet implementation class RegisterUser
 */
public class question2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static int COMPLETED_FORM_COUNT = 3;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public question2() {
        super();
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		
		out.println("<!doctype html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Register for ICT GradSchool Newsletter</title>");
		out.println("<meta charset='UTF-8' />");
		out.println("</head>");
		out.println("<body>");
		
		HttpSession session = request.getSession(); 
		
		Enumeration<String> params = request.getParameterNames();
		if(params.hasMoreElements()) {
			// This is the result of either a submit form, or clear form
			
			if (request.getParameter("cleardata") != null) {
				
				out.println("Need to clear the session information and produce a fresh, empty, form");
				
			}
			else {
				  	int numFieldsFilledIn  = storeFormDataSession(request);
					
					if (numFieldsFilledIn == 0) {
							out.println("<p>No information entered.</p>");
							out.println("<p>To return to the registration page, click <a href='question2'>here</a>.</p>"); 
						}
					  
					else if (numFieldsFilledIn != COMPLETED_FORM_COUNT) {
						out.println("<p>The data you've entered so far has been saved.</p>");
						out.println("<p>To continue your registration click <a href='question2'>here</a>.</p>"); 
					}
					else {
							out.println("<p>You have been registered!</p>");
						}
					}
		}
		else {
			
			createForm(session, out);
		}
		
	
		// close off the HTML page
		out.println("</body>");
		out.println("</html>");
	}
	
	
	

	
	protected int storeFormDataSession(HttpServletRequest request) {
	
		// Needs to be completed!
		
		int count = 0;
		
		// Store the three form fields as attributes in the session 
		// (for the ones that exist) 
		// Return a count of how may of the fields were stored
		
	    
	    return count;
	    
	}
	
	protected Properties getFormDataSession(HttpSession session) {
	 
		// Needs to be completed!
		
		Properties userDataProperties = new Properties();
	   
		// Retrieves the three form fields from the session 
		// and (for the ones that exist) stores them in a Properties class
				
	    return userDataProperties;
	}
	
	
	protected void createForm(HttpSession session, PrintWriter out) {
		
		Properties formFields = this.getFormDataSession(session);
			
		out.println("<form style='width:500px;margin:auto;' id='userform_id' name='userform' method='get' action='question2'>");
		
	
		out.println("<fieldset><legend>Register for the ICT GradSchool Newsletter:</legend>");
		
		
		out.println("<p>Form elements go here!</p>");

		// Generate appropriate input form fields for:
		//   firstname
		//   lastname
		//   email
		
		// 'Pre-fill' out a particular field in the form
		// if it exists in the variable 'formFields'
		
		
	
		out.println("<input type='submit' name='submit_button' id='submit_id' value='Register' /></p>");
		
		out.println("</fieldset>");
		out.println("</form>");
		
		out.println("<form  style='width:500px;margin:auto;' id='clearform_id' name='clearform' method='get' action='question2'>");
		out.println("<input type='hidden' name='cleardata' id='cleardata_id' value='clear' /></p>");
		out.println("<input type='submit' name='clear_button' id='clear_id' value='Clear Fields' /></p>");
		
		out.println("</form>");
		}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
