package ictgradschool.web.assignment04;

import org.json.simple.JSONObject;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;


/**
 * Servlet implementation class question1
 */
public class question1 extends HttpServlet {
    private static final long serialVersionUID = 1L;
    //create errorMessage variable to store error message
    private String errorMessage;


    /**
     * @see HttpServlet#HttpServlet()
     */
    public question1() {
        super();
    }

    /**
     * We set the directory that the transactions will be written to in the
     * during servlet configuration in web.xml
     */
    protected String transactionDir = null;

    /**
     * @param servletConfig a ServletConfig object containing information gathered when
     *                      the servlet was created, including information from web.xml
     */
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        this.transactionDir = servletConfig.getInitParameter("transaction-directory");
    }
    /** init(ServletConfig) => void **/


    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // You can uncomment the following line to check the Web server
        // if necessary
        //response.getWriter().append("Served at: ").append(request.getContextPath());

        PrintWriter out = response.getWriter();

        String refNum = request.getParameter("refNum");
        //create variables for account information and assign values to them
        String billingAddress = request.getParameter("address");
        String postCode = request.getParameter("postCode");
        String cardType = request.getParameter("cardType");
        String name = request.getParameter("cardName");
        String cardNum1 = request.getParameter("cardNo1");
        String cardNum2 = request.getParameter("cardNo2");
        String cardNum3 = request.getParameter("cardNo3");
        String cardNum4 = request.getParameter("cardNo4");


        // If a transaction directory hasn't been set in the web.xml, then
        // default to one within our web context (which might be in some
        // strange directory deep within .metadata in the workspace)
        if (this.transactionDir == null) {
            ServletContext servletContext = getServletContext();
            this.transactionDir = servletContext.getRealPath("/Transactions");
        }

        File jsonFile = new File(this.transactionDir, refNum + ".json");

        //create a new json object and store account information to it
        JSONObject jsonObject = new JSONObject();

        JSONObject addObject = new JSONObject();
        addObject.put("address", billingAddress);
        addObject.put("postCode", postCode);
        jsonObject.put("billingAddress", addObject);

        JSONObject cardObject = new JSONObject();
        cardObject.put("cardName", name);
        cardObject.put("cardType", cardType);
        cardObject.put("cardNo", cardNum1 + "-" + cardNum2 + "-" + cardNum3 + "-" + cardNum4);
        jsonObject.put("creditCard", cardObject);

        //call saveJSONObject method to save account information to a file
        boolean hasSaved = saveJSONObject(jsonFile, jsonObject);

        //print out return status and error message if error occurred
        if (hasSaved) {
            out.println("<p>Data saved: OK</p>");
            out.println("<p>Location to save json file = " + jsonFile + "</p>");
        }else {
            out.println("<p>An error occurred</p>");
            out.println("<p>Error message: " + this.errorMessage + "</p>");
        }

        // Remove the following line, and then start coding to solve
        // the task described in Question 1 of the test


    }
    /** doGet(HttpServletRequest, HttpServletResponse) => void **/


    /**
     * No special actions for POST so chains throught to doGet()
     *
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }
    /** doPost(HttpServletRequest, HttpServletResponse) => void **/


    /**
     * Writes the given JSONObject (in JSON format) to the specified file path
     *
     * @param file
     * @param jsonRecord
     * @return true if file written successfully, false otherwise
     */
    private boolean saveJSONObject(File file, JSONObject jsonRecord) {

        System.out.println("enter saveJSONObject method");
        boolean statusOK = true;

        if (file.exists()) {
            this.errorMessage = "Reference number already exists.";
            return false;
        }

        String json_string = JSONObject.toJSONString(jsonRecord);

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(json_string);

        } catch (IOException e) {
            this.errorMessage = e.getMessage();
            statusOK = false;
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException e) {
                this.errorMessage  = e.getMessage();
                statusOK = false;
            }
        }

        return statusOK;
    }
    /** saveJSONObject(File, JSONObject) => boolean **/
}
